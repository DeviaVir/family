var user = false,
	menu = 0
	

$( window ).load( function() {		

	if( !user ) {
		var app = $.sammy( function() {
	        this.get( '#!/page/:name', function() {
		        menu = this.params[ 'name' ]
		        $( '#main section' ).addClass( 'hide' ).removeClass( 'show' )
				$( '#' + menu ).addClass( 'show' ).removeClass( 'hide' )
				switch( menu ) {
					case 'home':
						$( 'body' ).swipe({
							swipeLeft: function() {
								$( 'footer .log_in' ).click()
							},
							swipeRight: function() {
								$( 'footer .sign_up' ).click()
							}
						})
					break;
					case 'log_in':
						$( 'body' ).swipe({
							swipeLeft: function() {
								$( 'footer .sign_up' ).click()
							},
							swipeRight: function() {
								$( 'footer .home' ).click()
							}
						})
					break;
					case 'sign_up':
						$( 'body' ).swipe({
							swipeLeft: function() {
								$( 'footer .home' ).click()
							},
							swipeRight: function() {
								$( 'footer .log_in' ).click()
							}
						})
					break;
				}
			})
		})
	
		// start the application
		app.run( '#!/page/home' )
	
	} else {
		// replace the footer
		$( 'footer .nouser' ).addClass( 'hide' ).next( 'user' ).removeClass( 'hide' )
		var app = $.sammy( function() {
	        this.get( '#!/user/:name', function() {
		        menu = this.params[ 'name' ]
		        $( '#main section' ).addClass( 'hide' ).removeClass( 'show' )
				$( '#' + menu ).addClass( 'show' ).removeClass( 'hide' )
				switch( menu ) {
					case 'family':
						$( 'body' ).swipe({
							swipeLeft: function() {
								$( 'footer .ping' ).click()
							},
							swipeRight: function() {
								$( 'footer .history' ).click()
							}
						})
					break;
					case 'ping':
						$( 'body' ).swipe({
							swipeLeft: function() {
								$( 'footer .history' ).click()
							},
							swipeRight: function() {
								$( 'footer .family' ).click()
							}
						})
					break;
					case 'history':
						$( 'body' ).swipe({
							swipeLeft: function() {
								$( 'footer .ping' ).click()
							},
							swipeRight: function() {
								$( 'footer .log_in' ).click()
							}
						})
					break;
				}
			})
		})
	
		// start the application
		app.run( '#!/user/family' )
	}
})